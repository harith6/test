from datetime import datetime, timedelta
import pickle
import psycopg2
import pyodbc

#Origin Data
ori_user = 'bcp_user'
ori_pass = 'Ug8U7zGZW6FM5XWb'
ori_database = 'prod_bangchak_line_bc'
ori_host = '172.19.108.30'
ori_port = 54320

ori_connection = psycopg2.connect(dbname=ori_database, user=ori_user, password=ori_pass, host=ori_host, port=ori_port)
ori_cursor = ori_connection.cursor()

#Destination Data
des_user = 'LINEADM'
des_pass = 'd@ta2taBLE47'
des_database = 'BCP-PDPA'
des_host = '172.19.103.88'
driver = 'ODBC Driver 17 for SQL Server'
des_connection = pyodbc.connect('DRIVER={' +driver + '};SERVER=' + des_host + ';DATABASE=' + des_database + ';UID=' + des_user + ';PWD=' + des_pass)
des_cursor = des_connection.cursor()

def check_data():
    #get last id was save in Destination
    des_cursor.execute("""
        SELECT id 
        from [BCP-PDPA].dbo.LineMember 
        order by id desc 
        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY 
        """)

    last_id = des_cursor.fetchone()

    #get last id was save in Origin
    ori_cursor.execute("""
        select id
        from membership_bangchaklinelogin
        order by id desc 
        limit 1
        """)

    latest_id = ori_cursor.fetchone()
    
    if int(last_id[0]) < int(latest_id[0]):
        print(str(last_id[0]) ,"<", str(latest_id[0]))
        load_data(last_id[0])
    else:
        print(str(last_id[0]) ,"=", str(latest_id[0]))

def load_data(last_id):
    sql = """
        select mb.id, mb.line_user_id, mm.first_name, mm.last_name, 
			CASE WHEN mb.member_number is null 
                THEN mm.member_number ELSE mb.member_number
                END AS member_number,
			CASE WHEN mb.member_card_no is null 
                THEN mm.member_card_no ELSE mb.member_card_no
                END AS member_card_no,
            CASE WHEN mb.phone_number is null or mb.phone_number = ''
                THEN mm.phone_number ELSE mb.phone_number
                END AS phone_number,
            CASE WHEN first_name is null and last_name is null 
                THEN 'no' ELSE 'yes'
                END AS is_from_registration,
            mb.created_at, mb.updated_at, mb.status, mm.date_of_birth, mm.citizen_id, mm.email
        from membership_memberregister mm 
        FULL OUTER join membership_bangchaklinelogin mb 
        on mb.line_user_id = mm.line_user_id
        where mb.id > """ + str(last_id) + """
        order by mb.id
        """
    ori_cursor.execute(sql)

    record = ori_cursor.fetchall()
    save_data(record)

def none_member(_id,mem_no,card_no):
    try:
        check = pickle.load(open('./none_member','rb'))
        check.append({"datetime":datetime.now(),"id":_id,"mem_no":mem_no,"card_no":card_no})
        for index, c in enumerate(check):
            if (datetime.now() - c['datetime']).days >= 7:
                check = check[:index] + check [index+1:]

        pickle.dump(check, open('./none_member','wb'))
    except:
        pickle.dump([{"datetime":datetime.now(),"id":_id,"mem_no":mem_no,"card_no":card_no}], open('./none_member','wb'))

def save_data(record):
    print("There are",len(record),"new records.")
    
    for r in record:
        if r[4] is None or r[5] is None:
            none_member(r[0],r[4],r[5])
            
        print("ID :",r[0])
        des_cursor.execute("""
                            INSERT INTO [BCP-PDPA].dbo.LineMember (id,Line_user_id,First_name,Last_name,Member_number,Member_Card_no,Phone_number,Is_from_registration,Created_at,Updated_at,Date_of_birth,Citizen_id,Email)
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);""",
                            r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[11],r[12],r[13]).rowcount
        des_connection.commit()
        print('Rows inserted :' + str(r[0]))

def check_update():
    check = pickle.load(open('./none_member','rb'))
    for index, c in enumerate(check):
        _id = c["id"]
        sql = """
                select mb.id, mb.line_user_id, mm.first_name, mm.last_name, 
                    CASE WHEN mb.member_number is null 
                        THEN mm.member_number ELSE mb.member_number
                        END AS member_number,
                    CASE WHEN mb.member_card_no is null 
                        THEN mm.member_card_no ELSE mb.member_card_no
                        END AS member_card_no,
                    CASE WHEN mb.phone_number is null or mb.phone_number = ''
                        THEN mm.phone_number ELSE mb.phone_number
                        END AS phone_number,
                    CASE WHEN first_name is null and last_name is null 
                        THEN 'no' ELSE 'yes'
                        END AS is_from_registration,
                    mb.created_at, mb.updated_at, mb.status, mm.date_of_birth, mm.citizen_id, mm.email
                from membership_memberregister mm 
                    FULL OUTER join membership_bangchaklinelogin mb 
                    on mb.line_user_id = mm.line_user_id
                where mb.id = """ + str(_id) + """
                order by mb.id
                """
        ori_cursor.execute(sql)
        new_data = ori_cursor.fetchone()
        
        if new_data is not None:
            if new_data[4] != c['mem_no'] or new_data[5] != c['card_no']:
                r = new_data
                des_cursor.execute("""
                                DELETE FROM [BCP-PDPA].dbo.LineMember
                                WHERE id=?;""",
                                r[0])
                des_connection.commit()
                des_cursor.execute("""
                                INSERT INTO [BCP-PDPA].dbo.LineMember (id,Line_user_id,First_name,Last_name,Member_number,Member_Card_no,Phone_number,Is_from_registration,Created_at,Updated_at,Date_of_birth,Citizen_id,Email)
                                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);""",
                                r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[11],r[12],r[13])
                des_connection.commit()
                print('Rows updated :' + str(r[0]))
        
        if new_data is None or (new_data[4] != None and new_data[5] != None):
            check = check[:index] + check [index+1:]
            pickle.dump(check, open('./none_member','wb'))

from airflow import DAG
from airflow.operators.python import PythonOperator

default_args = {
    'owner': 'devaum',
    'email': ['harith@looksocial.asia'],
    'retries': 5,
    'retry_delay': timedelta(minutes=2)
}


with DAG(
    dag_id='Bangchak_data_integration',
    default_args=default_args,
    description='',
    start_date=datetime(2022, 4, 19),
    schedule_interval='0 * * * *',
    catchup=False,
) as dag:

    task1 = PythonOperator(
        task_id='check_data',
        python_callable= check_data,
        dag=dag,
    )

    task2 = PythonOperator(
        task_id='check_update',
        python_callable= check_update,
        dag=dag,
    )

task1 >> task2
