from datetime import datetime, timedelta
import pickle
import psycopg2
import pyodbc

#Origin Data
ori_user = 'bcp_user'
ori_pass = 'Ug8U7zGZW6FM5XWb'
ori_database = 'prod_bangchak_line_bc'
ori_host = '172.19.108.30'
ori_port = 54320

ori_connection = psycopg2.connect(dbname=ori_database, user=ori_user, password=ori_pass, host=ori_host, port=ori_port)
ori_cursor = ori_connection.cursor()

#Destination Data
des_user = 'LINEADM'
des_pass = 'd@ta2taBLE47'
des_database = 'BCP-PDPA'
des_host = '172.19.103.88'
driver = 'ODBC Driver 17 for SQL Server'
des_connection = pyodbc.connect('DRIVER={' +driver + '};SERVER=' + des_host + ';DATABASE=' + des_database + ';UID=' + des_user + ';PWD=' + des_pass)
des_cursor = des_connection.cursor()

def load_data():
    ori_cursor.execute("""
        select id
        from line_members_export
        """)

    data = ori_cursor.fetchall()
    
    print("There are",len(data),"new records.")

    for d in data:
        save_data(d)

def save_data(r):
    des_cursor.execute("""
        SELECT id
        from [BCP-PDPA].dbo.LineMember 
        where Line_user_id = ? and Member_Card_no = ?
        order by id desc 
        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY 
        """,r[0],r[4])

    same_data = des_cursor.fetchall()

    if len(same_data) > 0:
        des_cursor.execute("""
                                DELETE FROM [BCP-PDPA].dbo.LineMember
                                WHERE id=?;""",same_data[0][0])
        des_connection.commit()
        des_cursor.execute("""
                            INSERT INTO [BCP-PDPA].dbo.LineMember (id,Line_user_id,First_name,Last_name,Member_number,Member_Card_no,Phone_number,Is_from_registration,Created_at,Updated_at,Date_of_birth,Citizen_id,Email)
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);""",
                            same_data[0][0],r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11]).rowcount
        des_connection.commit()
        print("update id:",same_data[0][0])
    else:
        des_cursor.execute("""SELECT COUNT(*) FROM [BCP-PDPA].dbo.LineMember;""")
        last_id = des_cursor.fetchall()

        des_cursor.execute("""
                            INSERT INTO [BCP-PDPA].dbo.LineMember (id,Line_user_id,First_name,Last_name,Member_number,Member_Card_no,Phone_number,Is_from_registration,Created_at,Updated_at,Date_of_birth,Citizen_id,Email)
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);""",
                            last_id[0][0],r[0],r[1],r[2],r[3],r[4],r[5],r[6],r[7],r[8],r[9],r[10],r[11]).rowcount
        des_connection.commit()
        print("insert id:",last_id[0][0])

from airflow import DAG
from airflow.operators.python import PythonOperator

default_args = {
    'owner': 'devaum',
    'email': ['harith@looksocial.asia'],
    'retries': 5,
    'retry_delay': timedelta(minutes=2)
}


with DAG(
    dag_id='Bangchak_data_integration',
    default_args=default_args,
    description='',
    start_date=datetime(2022, 4, 19),
    schedule_interval='0 * * * *',
    catchup=False,
) as dag:

    task1 = PythonOperator(
        task_id='check_data',
        python_callable= check_data,
        dag=dag,
    )

task1