from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash import BashOperator

import requests
from datetime import datetime

month = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "ฤศจิกายน", "ธันวาคม"]
cond = {"1":"ท้องฟ้าแจ่มใส",
        "2":"มีเมฆบางส่วน",
        "3":"เมฆเป็นส่วนมาก",
        "4":"มีเมฆมาก",
        "5":"ฝนตกเล็กน้อย",
        "6":"ฝนปานกลาง",
        "7":"ฝนตกหนัก",
        "8":"ฝนฟ้าคะนอง",
        "9":"อากาศหนาวจัด",
        "10":"อากาศหนาว",
        "11":"อากาศเย็น",
        "12":"อากาศร้อนจัด"}

url = "https://data.tmd.go.th/nwpapi/v1/forecast/location/daily/at?duration=2&lat=18.368815&lon=103.647313&fields=tc_max,tc_min,rh,ws10m,cond"

access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJjMzAwNmZjMmE2Njg5MjA2NjZhMmY4MzliMjhlMGY4Mjc1NTMwMzk4NDhjZjRlMmZmNDQ1MjdmNDI4M2FlOWMwYTA1NTYxMDA4NDM0NmU0In0.eyJhdWQiOiIyIiwianRpIjoiMmMzMDA2ZmMyYTY2ODkyMDY2NmEyZjgzOWIyOGUwZjgyNzU1MzAzOTg0OGNmNGUyZmY0NDUyN2Y0MjgzYWU5YzBhMDU1NjEwMDg0MzQ2ZTQiLCJpYXQiOjE2NDkzODc1ODIsIm5iZiI6MTY0OTM4NzU4MiwiZXhwIjoxNjgwOTIzNTgyLCJzdWIiOiIxODUyIiwic2NvcGVzIjpbXX0.FGAGoUN2s64vhijXUbMoe4t7DfbjL7rrPRvg4kyrleviYzrB47qLio-kBo2AY90F4PluMVu1CuicRhi1tciA5gKP1_ckdljkfRNr6KgPyoQU7EiG7Q70Oau4AcobUEhMzGdSp4SbnGHqLXICiTnc8yxaSBY8KnxaLAxP-L3403Bxx3PTLggrthCqW8zOX1rRhcu7BHGgUtZrfblKHkQmf8PLlVhI4oXrBWHQambxGhimfEQ8NZOClLfKyuJJ9k2VgNgrjE51o9wf8BC8gllG6F4bRjuPPfPEPvm-heH0XKNmiJ_DWuq_wWk8uWnOI1oEot_3ykpswQLcHiKye-BKS378Jd2JrgfcjCYFtgATCP2EHwh9cRC2U4cq1q444FIb1I9v2yr60u_rECo_v7bwxHxONDFGfQyJErwNtbihaZYtV7Fqup6qcvkhKIpRX_AQUnC8H39LQDUmo_KRnsbqF1yGn00_lCyCqDeC1ZFSh0YsRkWrS46HrOVFED7AI3boxFiXZlo1l_vm6E4QiQ7tdp8niZrZ3Gw5Er4USspAhlLY_7EytFXoKC-604ZQ1rIjmr1voLHWTH6LrdSBoZFMypuz39JZApTT0HojkXub1VtzjuCOKm8-hZ0cuqjU3b_SmI6P3Gwz1fwvFQq_6UVp6tOlebOvTCW85TrnEgOvwwY"

headers = {
    'accept': "application/json",
    'authorization': "Bearer " + access_token,
}

def get_data():
  response = requests.request("GET", url, headers=headers)

  weather = response.json()["WeatherForecasts"][0]["forecasts"][0]
  tomorrow = response.json()["WeatherForecasts"][0]["forecasts"][1]
  weather_date = datetime.strptime(weather["time"].split("T")[0], '%Y-%m-%d')

  output = "สภาพอากาศประจำวันที่ ", weather_date.day, " ", month[weather_date.month-1], " พ.ศ.", weather_date.year + 543
  output += "\\nโดยรวม\\t\\t", cond[str(weather["data"]["cond"])]
  output += "\\nอุณหภูมิสูงสุด\\t", str(weather["data"]["tc_max"]), "˚C"
  output += "\\nอุณหภูมิต่ำสุด\\t", str(weather["data"]["tc_min"]), "˚C"
  output += "\\nความชื้นสัมพัทธ์\\t", str(weather["data"]["rh"]), "%"
  output += "\\nความเร็วลม\\t", str(weather["data"]["ws10m"]), "m/s"
  output += "\\n####################################",""
  output += "\\nสภาพอากาศพรุ่งนี้\\t", cond[str(tomorrow["data"]["cond"])]

  return str("".join([str(o) for o in output]))


default_args = {
    'owner': 'devaum',
    'email': 'harith.s@ku.th',
    'retries': 5,
    'retry_delay': timedelta(minutes=2)
}


with DAG(
    dag_id='weather',
    default_args=default_args,
    description='This is our first dag that we write',
    start_date=datetime(2022, 4, 18),
    schedule_interval='0 0 * * *',
) as dag:
    task1 = BashOperator(
        task_id='first_task',
        bash_command="curl -i -H \"Accept: application/json\" -H \"Content-Type:application/json\" -X POST --data '{\"content\": \"" + get_data() + "\"}' https://discord.com/api/webhooks/957850061797220392/jLqqu96UW1mQ5PtxTkVenF3_igvTSZCB6-XgCXxmswyfO0U7aULd4ZNqMuAhacxvBkSs"
    )